import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Home from 'components/core/Home'
import Node from 'components/core/rbac/Node'
import Role from 'components/core/rbac/Role'
import RolePost from 'components/core/rbac/RolePost'
import User from 'components/core/rbac/User'

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {path: '/', component: Home},
    {path: '/sms/node', component: Node},
    {path: '/sms/role', component: Role},
    {path: '/sms/role/add', component: RolePost},
    {path: '/sms/role/update', component: RolePost},
    {path: '/sms/user', component: User}
  ]
})

export default router
